require 'test-unit'
require_relative 'hello_world'

class MyTest < Test::Unit::TestCase

  def setup
    @hello_world = HelloWorld.new
  end

  def test_statement
    assert_equal(@hello_world.statement, 'hello world')
  end
end